package com.example.db.batis;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

public class Message {

    private String msg;
    private Timestamp timestamp;

    public Message() {
    }

    public Message(String msg) {
        this.msg = msg;
        final Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        timestamp = new Timestamp(utc.getTimeInMillis());
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Message{" +
                "msg='" + msg + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
