package com.example.db.batis;

import org.apache.ibatis.annotations.Insert;

public interface MessageMapper {

    @Insert("INSERT INTO proj001(res_body, created_at) VALUES (#{msg}, #{timestamp})")
    void insertMessage(Message message);
}
