package com.example.db.batis;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;

import static com.example.utils.Const.BATIS_CONFIG_XML;
import static com.example.utils.Const.INSERT_METHOD;

public class BatisHelper {

    private final SqlSessionFactory factory;

    public BatisHelper() throws IOException {
        final Reader reader = Resources.getResourceAsReader(BATIS_CONFIG_XML);
        factory = new SqlSessionFactoryBuilder().build(reader);
        reader.close();
    }

    public void insertMessage(final String msg) {
        final SqlSession session = factory.openSession();

        try {
            final Message message = new Message(msg);

            session.update(INSERT_METHOD, message);
            session.commit();
        } finally {
            session.close();
        }
    }
}
