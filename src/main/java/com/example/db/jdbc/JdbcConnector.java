package com.example.db.jdbc;

import java.sql.*;
import java.util.Calendar;
import java.util.TimeZone;

import static com.example.utils.Const.*;

public class JdbcConnector {

    private final Connection connection;

    public JdbcConnector() throws SQLException {
        connection = DriverManager.getConnection(JDBC_URL, POSTGRES_PORT_USER_NAME, POSTGRES_PASSWORD);
    }

    public void saveInfo(final String msg) throws SQLException {
        final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INFO);
        final Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        final Timestamp timestamp = new Timestamp(utc.getTimeInMillis());

        System.out.println("utc = " + utc.getTimeInMillis() + " system = " + System.currentTimeMillis());

        preparedStatement.setString(1, msg);
        preparedStatement.setTimestamp(2, timestamp);
        preparedStatement.executeUpdate();
    }

    //fixme - must find place where I can call it
    public void closeConnection() throws SQLException {
        if (connection != null) connection.close();
    }
}
