package com.example;

import com.example.config.MyConfig;
import com.example.server.DiscardServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        try {
            final ApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
            final DiscardServer server = (DiscardServer) context.getBean("server");
            server.startNettyServer();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
