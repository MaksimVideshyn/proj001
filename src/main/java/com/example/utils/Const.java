package com.example.utils;

public interface Const {
    int PORT = 8080;
    String PONG = "pong";
    String PING = "ping";
    String LOCAL_HOST = "localhost";

    //postgres
    String JDBC_URL = "jdbc:postgresql://localhost:1234/test";
    int POSTGRES_PORT = 1234;
    String POSTGRES_PORT_USER_NAME = "postgres";
    String POSTGRES_PASSWORD = "1234";

    //SQL
    String SELECT_VERSION = "SELECT version()";
    String INSERT_INFO = "INSERT INTO proj001(res_body, created_at) VALUES(?, ?)";

    //batis
    String BATIS_CONFIG_XML = "mybatis-config.xml";
    String INSERT_METHOD = "insertMessage";
}
