package com.example.client;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import static com.example.utils.Const.*;

public class Client {

    public void createSocketClient() {
        final Socket client = new Socket();

        try {
            client.connect(new InetSocketAddress(LOCAL_HOST, PORT));

            System.out.println("Client, Client connect");

            final InputStream inputStream = client.getInputStream();
            final OutputStream outputStream = client.getOutputStream();
            final InputStreamReader reader = new InputStreamReader(inputStream);
            final StringBuilder builder = new StringBuilder();
            final char[] answer = new char[100];

            outputStream.write(PING.getBytes());
            outputStream.flush();

            while (reader.read(answer) != -1) {
                builder.append(answer);
                System.out.println("Client, answer = " + builder + "\n");
            }

        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
