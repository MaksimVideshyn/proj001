package com.example.server;

import com.example.db.batis.BatisHelper;
import com.example.db.jdbc.JdbcConnector;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.io.IOException;
import java.sql.SQLException;

import static com.example.utils.Const.PONG;

public class DiscardServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("DiscardServerHandler, msg = " + msg);

//        try {
//            final JdbcConnector dbConnector = new JdbcConnector();
//            dbConnector.saveInfo(msg.toString());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

        try {
            final BatisHelper batisHelper = new BatisHelper();
            batisHelper.insertMessage(msg.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        ctx.channel().writeAndFlush(PONG);

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
