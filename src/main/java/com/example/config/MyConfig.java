package com.example.config;

import com.example.client.Client;
import com.example.server.DiscardServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    @Bean("client")
    public Client getClient() {
        return new Client();
    }

    @Bean("server")
    public DiscardServer getServer(final Client client) {
        return new DiscardServer(new DiscardServer.Listener() {
            public void readyToConnect() {
                client.createSocketClient();
            }
        });
    }

}
